package com.example.hejuso.examendiejercicio2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    ListView listadoVista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listadoVista = findViewById(R.id.listadoNumeros);
        // Registramos el menu contextual
        registerForContextMenu(listadoVista);

        //String[] arrayNumeros = getResources().getStringArray(R.array.numeros);
        //List<String> listadoNumeros = Arrays.asList(arrayNumeros);

        // Preguntar al profesor
        //ArrayAdapter<String> selectorNombres = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1, listadoNumeros);

    }

    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){

        super.onCreateContextMenu(menu, v, menuInfo);
        // Nos guardamos en una variable la palabra seleccionada
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        String numero_seleccionado = ((TextView) info.targetView).getText().toString();

        // Asignamos el header
        menu.setHeaderTitle(numero_seleccionado);

        // Menu
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_numeros, menu);

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        // Con el adapter view nos guardamos la información del item seleccionado
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        long id_seleccion = info.id + 1;

        switch (item.getItemId()) {
            case R.id.anyadir:
                Toast.makeText(getApplicationContext(), "Se ha añadido un elemento detras del numero "+id_seleccion, Toast.LENGTH_LONG).show();
                return true;
            case R.id.eliminar:
                Toast.makeText(getApplicationContext(), "Se ha eliminado el número "+id_seleccion, Toast.LENGTH_LONG).show();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

}
